import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FacadeService } from '@dms/services/facade.service';
import { Router } from '@angular/router';
import { ThemePalette } from '@angular/material/core';
import { Routes } from '@dms/app/constants/routes';
import { TranslateService } from '@ngx-translate/core';
import { SignalRNotificationService } from '../../services/state-management/signal-rnotification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  readonly forgotPassword = Routes.forgotPassword;
  readonly register = Routes.register;

  form: FormGroup;
  errors: string[] = [];
  color: ThemePalette = 'accent';
  type: string = 'password';

  constructor(fb: FormBuilder,
    private facadeService: FacadeService,
    private router: Router,
    private translateService: TranslateService,
    private readonly signalrNotificationService: SignalRNotificationService) {
    this.form = fb.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  ngOnInit() {
    if (this.facadeService.accountService.isLoggedIn) {
      this.router.navigate(['app']);
    }
  }

  /**
   *
   * @param input
   */
  getError(input: string) {
    switch (input) {
      case 'email':
        if (this.form.get('email').hasError('required')) {
          return this.translateService.instant(`Email required`);
        }
        break;

      case 'password':
        if (this.form.get('password').hasError('required')) {
          return this.translateService.instant(`Password required`);
        }
        break;

      default:
        return '';
    }
  }

  /**
   * sign in
   *
   *
   * @param value
   */
  onSubmit(value) {

    this.facadeService.accountService.login(value).subscribe(res => {
      this.router.navigate(['app']);
    });
  }


  /**
   *@todo password to text and inverse
   *
   */
  togglePassword() {
    if (this.type == 'password') {
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }

}
