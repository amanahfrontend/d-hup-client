import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, ValidationErrors } from '@angular/forms';
import { FacadeService } from '@dms/app/services/facade.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Routes } from '@dms/app/constants/routes';
import { Country } from '@dms/app/models/settings/Country';
import { MatCheckboxChange } from '@angular/material';
import { PhoneNumberUtil } from 'google-libphonenumber';
import { SnackBar } from '@dms/app/utilities';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  private static phoneNumberUtil = PhoneNumberUtil.getInstance();

  readonly login = Routes.Login;
  readonly terms = Routes.terms;
  readonly privacyPolicy = Routes.privacyPolicy;

  form: FormGroup;

  errors: string[] = [];
  country: Country;
  acceptMessage: string = '';
  accept: boolean = false;
  numberMessage: string = '';

  /**
   *
   * @param fb
   * @param facadeService
   * @param router
   * @param activatedRoute
   */
  constructor(fb: FormBuilder,
    private facadeService: FacadeService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private snackBar: SnackBar,
    private translateService:TranslateService
  ) {
    this.form = fb.group({
      email: ['', [Validators.required, Validators.email]],
      fullName: ['', [Validators.required]],
      phoneNumber: ['', [Validators.required, Validators.minLength(4)]],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.pattern(/^(?=.*[0-9])(?=.*[!@#$%^&_*])(?=.*[A-Z])(?=.*[a-z])[a-zA-Z0-9!@#$%^&_*]/)])],
      confirmPassword: ['', [Validators.required, (value) => this.validateConfirmPassword(value)]],
    });
  }

  ngOnInit() {
  }

  /**
   * Display error msg
   *
   *
   * @param input
   */
  getError(input: string) {
    switch (input) {
      case 'email':
        if (this.form.get('email').hasError('required')) {
          return this.translateService.instant(`Email required`);
        }

        if (this.form.get('email').hasError('email')) {
          return this.translateService.instant(`Email not valid`);
        }
        break;

      case 'password':
        if (this.form.get('password').hasError('required')) {
          return   this.translateService.instant(`Password required`);
        } else if (this.form.get('password').hasError('minLength')) {
          return  this.translateService.instant(`Password must be at least 6 Characters`);

        } else if (this.form.get('password').hasError('pattern')) {
          return  this.translateService.instant(` This value is too short. It should have 8 characters or more. Password must be at least 8 characters long and must contain one lower case, one uppercase, one numeric and one special character.`);

        }
        break;

      case 'confirmPassword':
        if (this.form.get('confirmPassword').hasError('confirm')) {
          return this.translateService.instant(`Password and password confirmation don\'t match`);

        }
        break;

      case 'fullName':
        if (this.form.get('fullName').hasError('required')) {
          return   this.translateService.instant(`full Name required`);
        }
        break;

      case 'phoneNumber':
        if (this.form.get('phoneNumber').hasError('required')) {
          return this.translateService.instant(`Phone required`);
        }

        if (this.form.get('phoneNumber').hasError('minLength')) {
          return  this.translateService.instant(`Phone must be at least 4 Numbers`);
        }
        break;

      case 'terms':
        if (this.form.get('terms').hasError('requiredTrue')) {
          return this.translateService.instant(`You should accept our terms`);


        }
        break;

      default:
        return '';
    }
  }

  /**
   * create new account
   *
   *
   */
  onSubmit() {
    this.errors = [];
    this.acceptMessage = '';
    this.numberMessage = '';

    if (this.form.invalid) {
      return;
    }

    // validate number using google-libphonenumber
    let number: string = this.form.value['phoneNumber'];
    const phoneNumber = RegisterComponent.phoneNumberUtil.parseAndKeepRawInput(number.toString(), this.country.topLevel);
    const validNumber = RegisterComponent.phoneNumberUtil.isValidNumber(phoneNumber);

    if (!validNumber) {

        return this.numberMessage =  this.translateService.instant(`Not valid number`);;

    } else {
      this.numberMessage = '';
    }

    if (!this.accept) {
        return this.acceptMessage = this.translateService.instant(`Not valid number`);


    } else {
      this.acceptMessage = '';
    }

    let body = this.form.value;
    body.countryId = this.country.id;

    this.facadeService.accountService.register(body).subscribe(res => {
      if (res.succeeded === true) {
        this.snackBar.openSnackBar({ message:this.translateService.instant(`Successfully created`)
           , action: this.translateService.instant(`okay`), duration: 2500 });
        this.router.navigate([Routes.Login], { relativeTo: this.activatedRoute });
      }
    });
  }

  /**
   * confirm two passwords
   *
   *
   * @param control
   */
  private validateConfirmPassword(control: AbstractControl): ValidationErrors | null {
    return !this.form || control.value === this.form.controls.password.value ? null : { confirm: true };
  }

  /**
   *
   * @param country
   */
  onChangeCountry(country: Country) {
    this.country = country;
  }

  /**
   * handle accespt terms
   *
   *
   * @param event
   */
  onChangeAccept(event: MatCheckboxChange) {
    if (event.checked) {
      this.accept = true;
      this.acceptMessage = '';
    } else {
      this.accept = false;

        return this.acceptMessage = this.translateService.instant(`This field required`);

    }
  }

  /**
   *
   * @param event
   */
  onPast(event) {
    return;
  }
}
