import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, ValidationErrors } from '@angular/forms';
import { FacadeService } from '@dms/services/facade.service';
import { ActivatedRoute, Params } from '@angular/router';
import { ThemePalette } from '@angular/material/core';
import { Routes } from '@dms/app/constants/routes';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  showForm: boolean = false;
  success: boolean = false;
  progress: boolean = false;

  form: FormGroup;
  errors: string[] = [];
  color: ThemePalette = 'accent';

  readonly login = Routes.Login;


  /**
   *
   * @param fb
   * @param facadeService
   * @param activatedRoute
   */
  constructor(fb: FormBuilder,
    private facadeService: FacadeService,
    private activatedRoute: ActivatedRoute,
    private translateService:TranslateService) {
    this.form = fb.group({
      email: ['', [Validators.required, Validators.email]],
      newPassword: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required, (value) => this.validateConfirmPassword(value)]],
      token: ['', [Validators.required]],
    });
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      let email: string = params['email'];
      let token: string = params['token'];
      this.checkResetToken(email, token);
    });
  }

  /**
   * check rest token
   *
   *
   * @param email
   * @param token
   */
  checkResetToken(email: string, token: string) {
    this.progress = true;
    this.facadeService.accountService.checkResetToken({ email, token }).subscribe(res => {
      this.progress = false;
      if (res.succeeded === true) {
        this.form.get('email').setValue(email);
        this.form.get('token').setValue(token);
        this.showForm = true;
      } else {
        this.errors = res.errors.map(e => e.description);
      }
    },
      err => {
        this.progress = false;
      });
  }

  /**
   * validate form inputs
   *
   *
   * @param input
   */
  getError(input: string) {
    switch (input) {

      case 'newPassword':
        if (this.form.get('newPassword').hasError('required')) {
          return   this.translateService.instant(`Password required`);
        }
        break;

      case 'confirmPassword':
        if (this.form.get('confirmPassword').hasError('confirm')) {
          this.translateService.instant(`Password and password confirmation don\'t match`);
        }
        break;

      default:
        return '';
    }
  }



  /**
   * sign in
   *
   *
   * @param value
   */
  onSubmit() {
    if (!this.form.valid) {
      return;
    }

    this.facadeService.accountService.resetPassword(this.form.value).subscribe(res => {
      this.showForm = false;
      this.success = true;
    });
  }

  /**
   * confirm two passwords
   *
   *
   * @param control
   */
  private validateConfirmPassword(control: AbstractControl): ValidationErrors | null {
    return !this.form || control.value === this.form.controls.newPassword.value ? null : { confirm: true };
  }

}
