import { NgModule, isDevMode, Inject } from '@angular/core';
import { CommonModule, APP_BASE_HREF } from '@angular/common';

//Backend Endpoint settings
import { Settings, EnvironmentType } from '@dms/app/core/settings';
import { App } from '@dms/app/core/app';
import { environment } from '@dms/environments/environment';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { env } from 'process';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NgxUiLoaderModule
  ],
})
export class CoreModule {
  private app: Settings;

  constructor() {
    // if (isDevMode) {
    //   this.app = environment.settings;
    // }
    // App.Settings = this.app;
  }
}
