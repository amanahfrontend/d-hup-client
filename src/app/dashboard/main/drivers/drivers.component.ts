import { Component, OnInit, Output, EventEmitter, OnDestroy } from "@angular/core";
import { trigger, transition, style, animate } from "@angular/animations";
import { Images } from "@dms/app/constants/images";
import { Driver } from "@dms/app/models/settings/Driver";
import { FacadeService } from "@dms/app/services/facade.service";
import { App } from "@dms/app/core/app";
// import { FilterService } from '@dms/services/state-management/filter.service';
import { MatDialog } from "@angular/material";
import { ManageDriverComponent } from "@dms/components/settings/driver/manage-driver/manage-driver.component";
import { NotificationMessage, SignalRNotificationService } from "../../../services/state-management/signal-rnotification.service";
import { TaskstatusService } from "../../../services/state-management/taskstatus.service";
import { Subscription } from "rxjs";
import { MapMarkersService } from "@dms/app/services/state-management/map-markers.service";
import { MapMarker } from '@dms/app/models/main/tasks/map-marker';

@Component({
  selector: "app-drivers",
  templateUrl: "./drivers.component.html",
  styleUrls: ["./drivers.component.scss"],
  animations: [
    trigger("slideInOut", [
      transition(":enter", [
        style({ transform: "translateY(-100%)" }),
        animate("300ms ease-in", style({ transform: "translateY(0%)" })),
      ]),
    ]),
  ],
})
export class DriversComponent implements OnInit, OnDestroy {
  readonly image = Images.user;

  searchBy: string = "";
  searchAgent: boolean = false;
  driverDetails: boolean = false;
  drivers: Driver[] = [];
  busyDrivers: Driver[] = [];
  freeDrivers: Driver[] = [];
  inactiveDrivers: Driver[] = [];
  selectedDriver: Driver;
  subscription = new Subscription();

  ImageURL = App.driverImagesUrl;

  pageNumber: number = 1;
  pageSize: number = 50;

  length: number;
  totalCount: number;

  @Output() driverList: EventEmitter<any> = new EventEmitter<any>();
  @Output() close: EventEmitter<boolean> = new EventEmitter<boolean>();

  teamsIds: number[] = [];
  locale: string = 'en';
  markers: MapMarker[] = [];

  constructor(
    private facadeService: FacadeService,
    // private filterService: FilterService,
    public dialog: MatDialog,
    private signalrNotificationService: SignalRNotificationService,
    private TaskState: TaskstatusService,
    private mapMarkersService: MapMarkersService
  ) {
    this.facadeService.languageService.language.subscribe(lng => {
      this.locale = lng;
    });
  }

  ngOnInit() {

    this.TaskState.currentStatus.subscribe(x => {
      this.getDrivers();
    });

    this.TaskState.taskdeletetion.subscribe(x => {
      //this.getDrivers();
    });

    const taskNotificationMessages = [
      NotificationMessage.TaskSuccessful,
      NotificationMessage.TaskFailed,
      NotificationMessage.TaskStart,
    ];
    const driverNotificationMessages = [
      NotificationMessage.DriverLoggedin,
      NotificationMessage.DriverLoggedOut,
    ];

    this.subscription.add(
      this.signalrNotificationService.subscribeMultiple(
        taskNotificationMessages,
        (taskId, driver) => this.updateDriverStatus(driver))
    );
    this.subscription.add(
      this.signalrNotificationService.subscribeMultiple(
        driverNotificationMessages,
        driver => this.updateDriverStatus(driver))
    );
  }

  /**
   * fetch all Drivers
   *
   *
   */
  getDrivers() {

    const body = {
      pageNumber: this.pageNumber,
      pageSize: this.pageSize,
      searchBy: this.searchBy,
      teamIds: this.teamsIds,
    };

    this.subscription.add(this.facadeService.driverService.listByPagination(body).subscribe((result: any) => {
      this.drivers = result.result;
      this.length = result.totalCount;

      this.drivers.forEach(driver => {
        driver['fullName'] = `${driver.firstName} ${driver.lastName}`
      });

      this.busyDrivers = this.drivers.filter(driver => driver.agentStatusName == "Busy");

      this.inactiveDrivers = this.drivers.filter(driver =>
        driver.agentStatusName == "Blocked" ||
        driver.agentStatusName == "Unavailable" ||
        driver.agentStatusName == "Offline"
      );

      this.freeDrivers = this.drivers.filter(driver => driver.agentStatusName == "Available" || driver.agentStatusId == 6);

      this.buildMarkers();
    }));
  }

  buildMarkers() {
    this.markers = [];
    this.drivers.forEach((driver: Driver) => {
      this.markers.push({
        lat: +driver.latitude,
        lng: +driver.longitude,
        type: 'driver',
        isDriver: true,
        driver: driver,
      });
    });

    // this.mapMarkersService.changeMarkers([], false);
    this.mapMarkersService.changeMarkers(this.markers, true);
  }

  showDriverDetails(driver: Driver) {
    this.selectedDriver = driver;
    this.driverDetails = true;

    if (this.selectedDriver.latitude != null && this.selectedDriver.longitude != null) {
      const marker: MapMarker = {
        lat: +this.selectedDriver.latitude,
        lng: +this.selectedDriver.longitude,
        type: 'driver',
        isDriver: true,
        driver: driver
      }

      const findIndex = this.markers.indexOf(marker, 0);
      this.markers[findIndex] = marker;
      this.mapMarkersService.changeMarkers(this.markers, false);
    }
  }

  /**
   * close driver details / show drivers list
   *
   *
   */
  closeDriverDetails(event: boolean) {
    if (event) {
      this.driverDetails = false;
      this.mapMarkersService.changeMarkers(this.markers, false);
    }
  }
  /**
   * search tasks
   *
   *
   * @param event
   */
  onSearchSubmit(event) {
    this.searchBy = event.target.value;
    this.pageNumber = 1;
    this.getDrivers();
  }

  /**
   * reset search 
   * 
   * 
   */
  clearSearch() {
    this.searchBy = '';
    this.pageNumber = 1;
    this.getDrivers();
  }

  /**
   * close drivers list
   *
   *
   */
  onClose() {
    this.close.emit(true);
  }

  /**
   * add driver
   *
   *
   *@param operation
   */
  addDriverDialog(): void {
    const dialogRef = this.dialog.open(ManageDriverComponent, {
      width: "1200px",
      autoFocus: false,
      maxHeight: "95vh",
      data: {
        operation: "Add",
        driver: null,
      },
    });

    dialogRef.disableClose = true;
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
      } else {
        dialogRef.close();
      }
    });
  }


  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  updateDriverStatus(driver) {
    const currentdriver = this.drivers.find(x => x.id == driver.id);
    if (currentdriver.agentStatusId != driver.agentStatusId) {
      const driverindex = this.drivers.findIndex(x => x.id == driver.id);
      this.drivers.splice(driverindex, 1);
      this.drivers.push(driver);

      this.busyDrivers = this.drivers.filter(driver => driver.agentStatusName == "Busy");
      this.inactiveDrivers = this.drivers.filter(driver =>
        driver.agentStatusName == "Blocked" ||
        driver.agentStatusName == "Unavailable" ||
        driver.agentStatusName == "Offline");

      this.freeDrivers = this.drivers.filter(driver => driver.agentStatusName == "Available" || driver.agentStatusId == 6);
      this.buildMarkers();
    }
  }
}
