import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Images } from '@dms/app/constants/images';
import { MapMarkersService } from '@dms/app/services/state-management/map-markers.service';
import { MapMarker } from '@dms/app/models/main/tasks/map-marker';
import * as Leaflet from 'leaflet';
import { Subscription } from 'rxjs/internal/Subscription';

const TILE_LAYER = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';

const MAP_ZOOM = 6;
const MAX_ZOOM = 18;

const MAP_MARKER_ZOOM = 12;

@Component({
  selector: 'app-main-map',
  templateUrl: './main-map.component.html',
  styleUrls: ['./main-map.component.scss']
})
export class MainMapComponent implements OnInit, AfterViewInit, OnDestroy {

  private map: Leaflet.Map;
  private subscriptions = new Subscription();
  private markersGroup = Leaflet.layerGroup();

  constructor(private mapMarkersService: MapMarkersService) { }

  ngOnInit(): void {
    this.subscriptions.add(this.mapMarkersService.currentMarkers.subscribe((mapMarkers: MapMarker[]) => {

      // remove old
      this.markersGroup.clearLayers();

      // set new markers
      mapMarkers.forEach((marker: MapMarker) => {
        this.createMarker(marker, false);
      });
    }));

    // map zoom view
    this.subscriptions.add(this.mapMarkersService.currentView.subscribe((mapMarker: MapMarker) => {
      this.markersGroup.clearLayers();
      this.createMarker(mapMarker, true);
    }));
  }

  ngAfterViewInit(): void {
    this.initializeMap();
  }

  private initializeMap(): void {
    if (this.map !== null && this.map !== undefined) {
      this.map.remove();
    }

    this.map = Leaflet.map('map', {
      center: [29.2964, 47.9497],
      zoom: MAP_ZOOM
    });

    Leaflet.tileLayer(TILE_LAYER, {
      maxZoom: MAX_ZOOM,
    }).addTo(this.map);

    this.markersGroup.addTo(this.map);
  }

  /**
   * add marker on map for task / driver 
   * 
   * 
   * @param mapMarker 
   */
  private createMarker(marker: MapMarker, enableZoom: boolean): void {
    let icon = ''
    if (marker.type == 'driver') {
      icon = `assets/images/markers/${marker.driver.agentStatusName}.png`;
    } else if (marker.type == 'delivery') {
      icon = Images.deliveryIcon;
    } else {
      icon = Images.pickupIcon;
    }

    const latLng = new Leaflet.LatLng(marker.lat, marker.lng);
    let mapMarker: Leaflet.Marker = Leaflet.marker(latLng, {
      draggable: false,
      icon: Leaflet.icon({
        iconUrl: icon,
        iconSize: marker.isDriver ? [24, 24] : [60, 90],
        iconAnchor: [30 / 2, 35],
      })
    }).addTo(this.map);

    // zoom to marker view
    if (enableZoom || marker.task) {
      mapMarker.setLatLng({ lat: marker.lat, lng: marker.lng });
      this.map.setView({ lat: marker.lat, lng: marker.lng }, MAP_MARKER_ZOOM);
    }

    if (marker.isDriver) {
      mapMarker.bindTooltip(`${marker.driver.firstName} ${marker.driver.lastName}`, {
        permanent: true,
        direction: 'right'
      });
      mapMarker.addTo(this.markersGroup);
    } else {
      const address = marker.task && marker.task.address ? marker.task.address : `${marker.lat}, ${marker.lng} `;
      mapMarker.bindTooltip(address, { direction: 'right' });
      mapMarker.addTo(this.markersGroup);
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
