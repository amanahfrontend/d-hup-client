import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FacadeService } from '@dms/app/services/facade.service';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AccessControl, AccessControlGroup } from '@dms/app/models/settings/access-control/AccessControl.model';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { ThemePalette } from '@angular/material/core';
import { SnackBar } from '@dms/utilities/snakbar';
// import { relatedPermissions } from "./related-permissions";
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';

const DURATION = 2000;

@Component({
  selector: 'app-manage-role',
  templateUrl: './manage-role.component.html',
  styleUrls: ['./manage-role.component.scss']
})
export class ManageRoleComponent implements OnInit {
  color: ThemePalette = 'accent';
  isSubmitted: boolean = false;
  private role: AccessControl;
  allPermissions: AccessControlGroup[] = [];

  form: FormGroup;
  // profilePermissions: any[] = [];
  // rolePermissions = [];
  isEditMode = false;
  panelOpenState = false;

  // permissionFormArray: FormArray;
  step = 0;
  label: string;

  // relatedPermissions = relatedPermissions;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private facadeService: FacadeService,
    public dialogRef: MatDialogRef<ManageRoleComponent>,
    fb: FormBuilder,
    private snackBar: SnackBar,
    private translateService: TranslateService
  ) {

    this.form = fb.group({
      roleName: ['', [Validators.required]],
      permissions: fb.array([]),
    });

    if (data.role !== undefined) {
      this.form = fb.group({
        roleName: [data.role.roleName, [Validators.required]],
      });

      this.isEditMode = true;
      this.role = data.role;
      // this.rolePermissions = this.role.permissions;
    }
  }

  ngOnInit() {
    if (this.isEditMode) {
      this.label = this.translateService.instant('Edit');
    } else {
      this.label = this.translateService.instant('Add');
    }

    // Driver (agent)
    if (this.data.AccessControlType.type === 'driver') {
      this.agentPermissionsList();

      // manager
    } else if (this.data.AccessControlType.type === 'manager') {
      this.managerPermissionsList();
    }
  }

  /**
   * driver (agent) permissions list
   *
   *
   */
  agentPermissionsList() {
    this.facadeService.agentAccessControlService.listAllPermissions().subscribe((result) => {
      this.setCheckedPermissions(result, this.role && this.role.permissions);
      this.allPermissions = result;

    });
  }

  /**
   * manager permissions list
   *
   *
   */
  managerPermissionsList() {
    this.facadeService.managerAccessControlService.listAllPermissions().subscribe((result) => {
      this.setCheckedPermissions(result, this.role && this.role.permissions);
      this.allPermissions = result;
    });
  }

  private setCheckedPermissions(allPermissions: AccessControlGroup[], permissions: string[]) {
    if (!permissions) {
      return;
    }
    allPermissions.forEach(group =>
      group.permissions.forEach(permission =>
        permission.isChecked = this.role.permissions.includes(permission.value)));
  }
  /**
   * Update / Create role
   *
   *
   * @param role
   */
  onSubmit(role) {
    this.isSubmitted = true;
    if (this.data.role === undefined) {
      this.addRole(role);
    } else {
      this.editRole(role);
    }
  }

  /**
   * Add new role
   *
   *
   * @param role
   */
  private addRole(role) {
    this.role = new AccessControl();

    this.role.roleName = role.roleName;
    this.role.permissions = [];
    this.role.permissions = this.getCheckedPermissions();
    let createRole$: Observable<unknown>;
    if (this.data.AccessControlType.type === 'driver') {
      createRole$ = this.facadeService.agentAccessControlService.create(this.role)
    } else if (this.data.AccessControlType.type === 'manager') {
      createRole$ = this.facadeService.managerAccessControlService.create(this.role)
    } else {
      throw Error('Invalid AccessControlType');
    }
    createRole$.subscribe(() => {
      this.snackBar.openSnackBar({
        message: `${this.role.roleName}  ${this.translateService.instant('Role has been added successfully')}`,
        duration: DURATION,
        action: this.translateService.instant('Okay')
      });
      this.isSubmitted = false;
      this.closeDialog(this.data.AccessControlType.type);
    }, error => {
      this.isSubmitted = false;

    });
  }


  /**
   * Edit role
   *
   *
   * @param role
   */
  private editRole(role: AccessControl) {

    this.role.roleName = role.roleName;
    this.role.permissions = [];
    this.role.permissions = this.getCheckedPermissions();

    // driver (agent)
    if (this.data.AccessControlType.type === 'driver') {
      this.facadeService.agentAccessControlService.update(this.role).subscribe((role: any) => {
        this.snackBar.openSnackBar({
          message: `${this.role.roleName} ${this.translateService.instant('Role has been updated successfully')}`,
          duration: DURATION, action: 'Okay'
        });
        this.closeDialog(role);
        this.isSubmitted = false;
        this.closeDialog('driver');
      });
    }

    // manger
    if (this.data.AccessControlType.type === 'manager') {
      this.facadeService.managerAccessControlService.update(this.role).subscribe((role: any) => {
        this.snackBar.openSnackBar({
          message: `${this.role.roleName} ${this.translateService.instant('Role has been updated successfully')}`,
          duration: DURATION, action: 'Okay'
        });
        this.closeDialog(role);
        this.isSubmitted = false;
        this.closeDialog('manager');
      });
    }
  }

  /**
   * close dialog after edit / create
   *
   *
   * @param role
   */
  closeDialog(type: string) {
    this.dialogRef.close(type);
  }

  cancel(): void {
    this.dialogRef.close();
  }

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  private getCheckedPermissions() {
    return this.allPermissions.reduce((previous, current) =>
      [
        ...previous,
        ...current.permissions
          .filter(permission => permission.isChecked)
          .map(permission => permission.value)],
      []);
  }
}
