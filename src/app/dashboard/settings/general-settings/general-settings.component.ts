import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FacadeService } from '@dms/app/services/facade.service';
import { SnackBar } from '@dms/app/utilities';
import { TranslateService } from '@ngx-translate/core';

interface KeyValue {
  id?: number;
  groupId?: number;
  settingKey: string;
  value?: string;
}

@Component({
  selector: 'app-general-settings',
  templateUrl: './general-settings.component.html',
  styleUrls: ['./general-settings.component.scss']
})
export class GeneralSettingsComponent implements OnInit {
  form: FormGroup;

  settings: KeyValue[] = [];

  constructor(fb: FormBuilder,
    private readonly facadeService: FacadeService,
    private snackBar: SnackBar,
    private translateService: TranslateService) {
    this.form = fb.group({
      DriverLoginRequestExpiration: ['', [Validators.required]],
      ReachedDistanceRestriction: ['', [Validators.required]],
    });
  }

  ngOnInit() {
    // 2 for general settings (enum backend)
    this.facadeService.generalSettingsService.settingsByGroupId(2).subscribe((res: any) => {

      this.settings = res;
      this.settings.forEach((keyValue: KeyValue) => {

        // DriverLoginRequestExpiration
        if (keyValue.settingKey == 'DriverLoginRequestExpiration') {
          this.form.patchValue({ DriverLoginRequestExpiration: keyValue.value });
        }

        // ReachedDistanceRestriction
        if (keyValue.settingKey == 'ReachedDistanceRestriction') {
          this.form.patchValue({ ReachedDistanceRestriction: keyValue.value });
        }
      });
    });
  }

  /**
   * update settings
   * 
   * 
   */
  updateSettings() {
    const body = this.form.value;

    this.settings.forEach(keyValue => {
      if (keyValue.settingKey == 'DriverLoginRequestExpiration') {
        keyValue.value = body['DriverLoginRequestExpiration'];
      }

      if (keyValue.settingKey == 'ReachedDistanceRestriction') {
        keyValue.value = body['ReachedDistanceRestriction'];
      }
    });

    this.facadeService.generalSettingsService.updateSettings(this.settings).subscribe(res => {
      this.snackBar.openSnackBar({
        message: this.translateService.instant('General settings updated successfully!'),
        action: this.translateService.instant('okay'),
        duration: 2500
      });
    });
  }
}
