import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotificationsComponent } from './notifications.component';
import { AllNotificationsComponent } from './all-notifications/all-notifications.component';

const routes: Routes = [
  {
    path: '',
    component: NotificationsComponent
  },
  {
    path: 'all',
    component: AllNotificationsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotificationsRoutingModule { }
