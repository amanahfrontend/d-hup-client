import { Component, OnInit } from '@angular/core';

import { NotificationsSettingProperty, NotificationsSettingvalues, ServicesService } from './notifications.service';

export interface MatTableRow {
  events: string;
  admin: tableProperty;
  adminNotifyBy: selector;
  manager: tableProperty;
  managerNotifyBy: selector;

}
export interface selector {
  data: selectorData[];
  defaultValue: selectorData;
  key: string;
  GroupId: string;
}
export interface selectorData {
  text: string;
  value: string;
}
export interface tableProperty {
  key: string,
  value: boolean,
  GroupId: string
}


@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})

export class NotificationsComponent implements OnInit {

  displayedColumns: string[] = ['events', 'admin', 'adminNotifyBy', 'manager', 'managerNotifyBy'];
  pickupDataSource = [];
  deliveryDataSource = [];
  generalNotificationSource = [];
  page: number = 10;
  dropdownlist: selector = { defaultValue: null, data: [], key: null, GroupId: null };
  _servicesService: ServicesService;

  constructor(servicesService: ServicesService) {
    this._servicesService = servicesService;
  }

  ngOnInit() {
    this.dropdownlist = {
      key: null,
      GroupId: null,
      defaultValue: { text: NotificationsSettingvalues[NotificationsSettingvalues.notselected], value: NotificationsSettingvalues.notselected.toString() },
      data: [
        //  { key: "Select notification method", value: NotificationsSettingvalues.notselected.toString() },
        { text: NotificationsSettingvalues[NotificationsSettingvalues.both], value: NotificationsSettingvalues.both.toString() },
        { text: NotificationsSettingvalues[NotificationsSettingvalues.email], value: NotificationsSettingvalues.email.toString() },
        { text: NotificationsSettingvalues[NotificationsSettingvalues.sms], value: NotificationsSettingvalues.sms.toString() },

      ]
    };

    //#region  build table Pickup Notifications    
    //get data from server

    this.pickupDataSource = [];
    let PickupDataList = [];

    this._servicesService.GetPickupNotificationsEvents().subscribe(p => {
      PickupDataList.push(p);
    }, err => {
    }, () => {
      this.pickupDataSource = this.MappObjectList(PickupDataList, "PickupNotification");
    });

    //#endregion 

    //#region  build table delivery Notifications    
    //get data from server


    //map data 
    this.deliveryDataSource = [];
    let deliveryDataList = [];
    this._servicesService.GetDeliveryNotificationsEvents().subscribe(p => {
      deliveryDataList.push(p);
    }, err => {
    }, () => {
      let deliveryDataMaped = this.MappObjectList(deliveryDataList, "DeliveryNotification");
      this.deliveryDataSource = deliveryDataMaped;
    })

    //#endregion 


    //#region  build table Pickup Notifications    
    //get data from server
    this.generalNotificationSource = [];
    let generalNotificationList = [];
    this._servicesService.GetGeneralNotification().subscribe(p => {
      generalNotificationList.push(p);
    }, err => {
    }, () => {
      let generalNotificationMaped = this.MappObjectList(generalNotificationList, "GeneralNotification");
      this.generalNotificationSource = generalNotificationMaped;
    })
    //#endregion 
  }

  OnSelectedChange(event, item) {
    //validate key object
    if (item == undefined && event.element && item.key == undefined || item == undefined && event.element && item.key == null || item == undefined && event.element && item.key == "") {
      return;
    }

    //extract key
    let key: string = item.key;
    //validate value
    if (event == undefined && event.value == undefined || event == undefined && event.value == null || event && event.value == "") {
      return;
    }
    let value: string = event.value

    this._servicesService.UpdateKeyValue(key, value, "string", item.GroupId);
  }

  /**
   * 
   * @param event 
   * @param item 
   */
  OnSelectedchecked(event, item) {
    //validate key object
    item.key
    if (item == undefined && event.element && item.key == undefined || item == undefined && event.element && item.key == null || item && event.element && item.key == "") {
      return;
    }

    //extract key
    let key: string = item.key;
    //validate value
    if (event == undefined && event.checked == undefined || event == undefined && event.checked == null || event == undefined && event.checked == "") {
      return;
    }
    let value: string = event.checked

    this._servicesService.UpdateKeyValue(key, value, "boolean", item.GroupId);
  }

  /**
   * 
   * @param Propertes 
   * @param GroupId 
   */
  MappObjectList(Propertes: NotificationsSettingProperty[], GroupId: string): MatTableRow[] {
    let dataTable: MatTableRow[] = [];
    Propertes.forEach(prop => {
      let dropdownlistForAdmin: selector = {
        key: prop.keyAdmin,
        GroupId: GroupId,
        data: this.dropdownlist.data.map(x => Object.assign({}, x)),
        defaultValue: { text: NotificationsSettingvalues[prop.valueAdmin], value: prop.valueAdmin.toString() }
      };

      let dropdownlistForManager: selector = {
        key: prop.keyManager,
        GroupId: GroupId,
        data: this.dropdownlist.data.map(x => Object.assign({}, x)),
        defaultValue: { text: NotificationsSettingvalues[prop.valueManager], value: prop.valueManager.toString() }
      };

      dataTable.push({
        events: prop.DisplayName,
        admin: { key: prop.keyIsAdminEnabled, value: prop.IsAdminEnabled, GroupId },
        adminNotifyBy: dropdownlistForAdmin,
        manager: { key: prop.keyIsManagerEnabled, value: prop.IsManagerEnabled, GroupId },
        managerNotifyBy: dropdownlistForManager
      });
    });

    return dataTable;
  }
}
