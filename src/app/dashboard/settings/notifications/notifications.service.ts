import { Injectable } from '@angular/core';
import { HttpClientService } from '@dms/app/core/http-client/http-client.service';
import { Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})

export class ServicesService {
  private EndPoint = 'Settings';
  _httpClientService: HttpClientService;

  constructor(httpClientService: HttpClientService) {
    this._httpClientService = httpClientService;
  }
  private DefaultDeliveryNotificationsEvents(): NotificationsSettingProperty[] {
    let DeliveryNotificationsPropertyList: NotificationsSettingProperty[] = [
      { DisplayName: 'Accept Task', keyIsAdminEnabled: 'IsDeliveryAdminAcceptTaskEnabled', IsAdminEnabled: false, keyAdmin: 'DeliveryAdminAcceptTask', valueAdmin: NotificationsSettingvalues.email, keyIsManagerEnabled: "IsDeliveryManagerAcceptTaskEnabled", IsManagerEnabled: false, keyManager: 'DeliveryManagerAcceptTask', valueManager: NotificationsSettingvalues.email },
      { DisplayName: 'Decline Task', keyIsAdminEnabled: 'IsDeliveryAdminDeclineTaskEnabled', IsAdminEnabled: false, keyAdmin: 'DeliveryAdminDeclineTask', valueAdmin: NotificationsSettingvalues.email, keyIsManagerEnabled: "IsDeliveryManagerDeclineTaskEnabled", IsManagerEnabled: false, keyManager: 'DeliveryManagerDeclineTask', valueManager: NotificationsSettingvalues.email },
      { DisplayName: 'Start Task', keyIsAdminEnabled: 'IsDeliveryAdminStartTaskEnabled', IsAdminEnabled: false, keyAdmin: 'DeliveryAdminStartTask', valueAdmin: NotificationsSettingvalues.email, keyIsManagerEnabled: "IsDeliveryManagerStartTaskEnabled", IsManagerEnabled: false, keyManager: 'DeliveryManagerStartTask', valueManager: NotificationsSettingvalues.email },
      { DisplayName: 'Cancel Task', keyIsAdminEnabled: 'IsDeliveryAdminCancelTaskEnabled', IsAdminEnabled: false, keyAdmin: 'DeliveryAdminCancelTask', valueAdmin: NotificationsSettingvalues.email, keyIsManagerEnabled: "IsDeliveryManagerCancelTaskEnabled", IsManagerEnabled: false, keyManager: 'DeliveryManagerCancelTask', valueManager: NotificationsSettingvalues.email },
      { DisplayName: 'Successful Task', keyIsAdminEnabled: 'IsDeliveryAdminSuccessfulTaskEnabled', IsAdminEnabled: false, keyAdmin: 'DeliveryAdminSuccessfulTask', valueAdmin: NotificationsSettingvalues.email, keyIsManagerEnabled: "IsDeliveryManagerSuccessfulTaskEnabled", IsManagerEnabled: false, keyManager: 'DeliveryManagerSuccessfulTask', valueManager: NotificationsSettingvalues.email },
      { DisplayName: 'Failed Task', keyIsAdminEnabled: 'IsDeliveryAdminFailedTaskEnabled', IsAdminEnabled: false, keyAdmin: 'DeliveryAdminFailedTask', valueAdmin: NotificationsSettingvalues.email, keyIsManagerEnabled: "IsDeliveryManagerFailedTaskEnabled", IsManagerEnabled: false, keyManager: 'DeliveryManagerFailedTask', valueManager: NotificationsSettingvalues.email },
      { DisplayName: 'Add Note', keyIsAdminEnabled: 'IsDeliveryAdminAddNoteAdminEnabled', IsAdminEnabled: false, keyAdmin: 'DeliveryAdminAddNoteAdmin', valueAdmin: NotificationsSettingvalues.email, keyIsManagerEnabled: "IsDeliveryManagerAddNoteEnabled", IsManagerEnabled: false, keyManager: 'DeliveryManagerAddNote', valueManager: NotificationsSettingvalues.email },
      { DisplayName: 'Add Signature', keyIsAdminEnabled: 'IsDeliveryAdminAddSignatureEnabled', IsAdminEnabled: false, keyAdmin: 'DeliveryAdminAddSignature', valueAdmin: NotificationsSettingvalues.email, keyIsManagerEnabled: "IsDeliveryManagerAddSignatureEnabled", IsManagerEnabled: false, keyManager: 'DeliveryManagerAddSignature', valueManager: NotificationsSettingvalues.email },
      { DisplayName: 'Add Image', keyIsAdminEnabled: 'IsDeliveryAdminAddImageEnabled', IsAdminEnabled: false, keyAdmin: 'DeliveryAdminAddImage', valueAdmin: NotificationsSettingvalues.email, keyIsManagerEnabled: "IsDeliveryManagerAddImageEnabled", IsManagerEnabled: false, keyManager: 'DeliveryManagerAddImage', valueManager: NotificationsSettingvalues.email },

    ];
    return DeliveryNotificationsPropertyList

  }
  DefaultPickupNotificationsEvents(): NotificationsSettingProperty[] {
    let pickupNotificationsPropertyList: NotificationsSettingProperty[] = [
      { DisplayName: 'Accept Task', keyIsAdminEnabled: 'IsPickupAdminAcceptTaskEnabled', IsAdminEnabled: false, keyAdmin: 'PickupAdminAcceptTask', valueAdmin: NotificationsSettingvalues.email, keyIsManagerEnabled: "IsPickupManagerAcceptTaskEnabled", IsManagerEnabled: false, keyManager: 'PickupManagerAcceptTask', valueManager: NotificationsSettingvalues.email },
      { DisplayName: 'Decline Task', keyIsAdminEnabled: 'IsPickupAdminDeclineTaskEnabled', IsAdminEnabled: false, keyAdmin: 'PickupAdminDeclineTask', valueAdmin: NotificationsSettingvalues.email, keyIsManagerEnabled: "IsPickupManagerDeclineTaskEnabled", IsManagerEnabled: false, keyManager: 'PickupManagerDeclineTask', valueManager: NotificationsSettingvalues.email },
      { DisplayName: 'Start Task', keyIsAdminEnabled: 'IsPickupAdminStartTaskEnabled', IsAdminEnabled: false, keyAdmin: 'PickupAdminStartTask', valueAdmin: NotificationsSettingvalues.email, keyIsManagerEnabled: "IsPickupManagerStartTaskEnabled", IsManagerEnabled: false, keyManager: 'PickupManagerStartTask', valueManager: NotificationsSettingvalues.email },
      { DisplayName: 'Cancel Task', keyIsAdminEnabled: 'IsPickupAdminCancelTaskEnabled', IsAdminEnabled: false, keyAdmin: 'PickupAdminCancelTask', valueAdmin: NotificationsSettingvalues.email, keyIsManagerEnabled: "IsPickupManagerCancelTaskEnabled", IsManagerEnabled: false, keyManager: 'PickupManagerCancelTask', valueManager: NotificationsSettingvalues.email },
      { DisplayName: 'Successful Task', keyIsAdminEnabled: 'IsPickupAdminSuccessfulTaskEnabled', IsAdminEnabled: false, keyAdmin: 'PickupAdminSuccessfulTask', valueAdmin: NotificationsSettingvalues.email, keyIsManagerEnabled: "IsPickupManagerSuccessfulTaskEnabled", IsManagerEnabled: false, keyManager: 'PickupManagerSuccessfulTask', valueManager: NotificationsSettingvalues.email },
      { DisplayName: 'Failed Task', keyIsAdminEnabled: 'IsPickupAdminFailedTaskEnabled', IsAdminEnabled: false, keyAdmin: 'PickupAdminFailedTask', valueAdmin: NotificationsSettingvalues.email, keyIsManagerEnabled: "IsPickupManagerFailedTaskEnabled", IsManagerEnabled: false, keyManager: 'PickupManagerFailedTask', valueManager: NotificationsSettingvalues.email },
      { DisplayName: 'Add Note', keyIsAdminEnabled: 'IsPickupAdminAddNoteAdminEnabled', IsAdminEnabled: false, keyAdmin: 'PickupAdminAddNoteAdmin', valueAdmin: NotificationsSettingvalues.email, keyIsManagerEnabled: "IsPickupManagerAddNoteEnabled", IsManagerEnabled: false, keyManager: 'PickupManagerAddNote', valueManager: NotificationsSettingvalues.email },
      { DisplayName: 'Add Signature', keyIsAdminEnabled: 'IsPickupAdminAddSignatureEnabled', IsAdminEnabled: false, keyAdmin: 'PickupAdminAddSignature', valueAdmin: NotificationsSettingvalues.email, keyIsManagerEnabled: "IsPickupManagerAddSignatureEnabled", IsManagerEnabled: false, keyManager: 'PickupManagerAddSignature', valueManager: NotificationsSettingvalues.email },
      { DisplayName: 'Add Image', keyIsAdminEnabled: 'IsPickupAdminAddImageEnabled', IsAdminEnabled: false, keyAdmin: 'PickupAdminAddImage', valueAdmin: NotificationsSettingvalues.email, keyIsManagerEnabled: "IsPickupManagerAddImageEnabled", IsManagerEnabled: false, keyManager: 'PickupManagerAddImage', valueManager: NotificationsSettingvalues.email },

    ];
    return pickupNotificationsPropertyList

  }
  private DefaultGeneralNotification(): NotificationsSettingProperty[] {
    let generalNotificationPropertyList: NotificationsSettingProperty[] = [
      { DisplayName: 'Auto allocation started', keyIsAdminEnabled: 'IsGeneralAdminAutoAllocationStartedEnabled', IsAdminEnabled: false, keyAdmin: 'GeneralAdminAutoAllocationStarted', valueAdmin: NotificationsSettingvalues.email, keyIsManagerEnabled: "IsGeneralManagerAutoAllocationStartedEnabled", IsManagerEnabled: false, keyManager: 'GeneralManagerAutoAllocationStarted', valueManager: NotificationsSettingvalues.email },
      { DisplayName: 'Auto allocation failed', keyIsAdminEnabled: 'IsGeneralAdminAutoAllocationFailedEnabled', IsAdminEnabled: false, keyAdmin: 'GeneralAdminAutoAllocationFailed', valueAdmin: NotificationsSettingvalues.email, keyIsManagerEnabled: "IsDGeneralManagerAutoAllocationFailedEnabled", IsManagerEnabled: false, keyManager: 'GeneralManagerAutoAllocationFailed', valueManager: NotificationsSettingvalues.email },
    ];
    return generalNotificationPropertyList

  }
  public GetDeliveryNotificationsEvents(): Observable<NotificationsSettingProperty> {

    //call server
    let GroupId = "DeliveryNotification"
    return this.GetSettingEventsFromServer(GroupId).pipe(
      mergeMap(data => {
        if (data.length == 0) {
          return this.DefaultDeliveryNotificationsEvents();

        }
        let Mappeddata = this.MapOjbect(data, SettingServerGroupTypes.DeliveryNotification);
        return Mappeddata;
      })
    );
  }
  public GetPickupNotificationsEvents(): Observable<NotificationsSettingProperty> {
    //call server
    let GroupId = "PickupNotification"
    return this.GetSettingEventsFromServer(GroupId).pipe(

      mergeMap(data => {
        if (data.length == 0) {
          return this.DefaultPickupNotificationsEvents();

        }
        let Mappeddata = this.MapOjbect(data, SettingServerGroupTypes.PickupNotification);
        return Mappeddata;
      })
    );
  }
  public GetGeneralNotification(): Observable<NotificationsSettingProperty> {
    //call server
    let GroupId = "GeneralNotification"
    return this.GetSettingEventsFromServer(GroupId).pipe(
      mergeMap(data => {
        if (data.length == 0) {

          return this.DefaultGeneralNotification();
        }
        let Mappeddata = this.MapOjbect(data, SettingServerGroupTypes.GeneralNotification);
        return Mappeddata;
      })
    );
  }
  // public GetPickupNotificationsEvents(): NotificationsSettingProperty[] {

  //   let data: NotificationsSettingProperty[] = [];
  //   //call servere
  //   //if not exist 
  //   data = this.DefaultPickupNotificationsEvents();
  //   return data;
  // }
  // public GetGeneralNotification(): NotificationsSettingProperty[] {

  //   let data: NotificationsSettingProperty[] = [];
  //   //call servere
  //   //if not exist 
  //   data = this.DefaultGeneralNotification();
  //   return data;
  // }
  private GetSettingEventsFromServer(GroupId: string): Observable<NotificationsSettingProperty[]> {

    return this._httpClientService.get(`${this.EndPoint}/GetSettingByGroupId?GroupId=${GroupId}`)
  }
  private MapOjbect(data: any, settingServerGroupTypes: SettingServerGroupTypes) {
    let NotificationsPropertyList: NotificationsSettingProperty[]
    if (settingServerGroupTypes == SettingServerGroupTypes.DeliveryNotification) {
      NotificationsPropertyList = [
        { DisplayName: 'Accept Task', keyIsAdminEnabled: 'IsDeliveryAdminAcceptTaskEnabled', IsAdminEnabled: this.extractValueByKey(data, "IsDeliveryAdminAcceptTaskEnabled", SettingServerGroupTypes.DeliveryNotification), keyAdmin: 'DeliveryAdminAcceptTask', valueAdmin: this.extractValueByKey(data, "DeliveryAdminAcceptTask", SettingServerGroupTypes.DeliveryNotification), keyIsManagerEnabled: "IsDeliveryManagerAcceptTaskEnabled", IsManagerEnabled: this.extractValueByKey(data, "IsDeliveryManagerAcceptTaskEnabled", SettingServerGroupTypes.DeliveryNotification), keyManager: 'DeliveryManagerAcceptTask', valueManager: this.extractValueByKey(data, "DeliveryManagerAcceptTask", SettingServerGroupTypes.DeliveryNotification) },
        { DisplayName: 'Decline Task', keyIsAdminEnabled: 'IsDeliveryAdminDeclineTaskEnabled', IsAdminEnabled: this.extractValueByKey(data, "IsDeliveryAdminDeclineTaskEnabled", SettingServerGroupTypes.DeliveryNotification), keyAdmin: 'DeliveryAdminDeclineTask', valueAdmin: this.extractValueByKey(data, "DeliveryAdminDeclineTask", SettingServerGroupTypes.DeliveryNotification), keyIsManagerEnabled: "IsDeliveryManagerDeclineTaskEnabled", IsManagerEnabled: this.extractValueByKey(data, "IsDeliveryManagerDeclineTaskEnabled", SettingServerGroupTypes.DeliveryNotification), keyManager: 'DeliveryManagerDeclineTask', valueManager: this.extractValueByKey(data, "DeliveryManagerDeclineTask", SettingServerGroupTypes.DeliveryNotification) },
        { DisplayName: 'Start Task', keyIsAdminEnabled: 'IsDeliveryAdminStartTaskEnabled', IsAdminEnabled: this.extractValueByKey(data, "IsDeliveryAdminStartTaskEnabled", SettingServerGroupTypes.DeliveryNotification), keyAdmin: 'DeliveryAdminStartTask', valueAdmin: this.extractValueByKey(data, "DeliveryAdminStartTask", SettingServerGroupTypes.DeliveryNotification), keyIsManagerEnabled: "IsDeliveryManagerStartTaskEnabled", IsManagerEnabled: this.extractValueByKey(data, "IsDeliveryManagerStartTaskEnabled", SettingServerGroupTypes.DeliveryNotification), keyManager: 'DeliveryManagerStartTask', valueManager: this.extractValueByKey(data, "DeliveryManagerStartTask", SettingServerGroupTypes.DeliveryNotification) },
        { DisplayName: 'Cancel Task', keyIsAdminEnabled: 'IsDeliveryAdminCancelTaskEnabled', IsAdminEnabled: this.extractValueByKey(data, "IsDeliveryAdminCancelTaskEnabled", SettingServerGroupTypes.DeliveryNotification), keyAdmin: 'DeliveryAdminCancelTask', valueAdmin: this.extractValueByKey(data, "DeliveryAdminCancelTask", SettingServerGroupTypes.DeliveryNotification), keyIsManagerEnabled: "IsDeliveryManagerCancelTaskEnabled", IsManagerEnabled: this.extractValueByKey(data, "IsDeliveryManagerCancelTaskEnabled", SettingServerGroupTypes.DeliveryNotification), keyManager: 'DeliveryManagerCancelTask', valueManager: this.extractValueByKey(data, "DeliveryManagerCancelTask", SettingServerGroupTypes.DeliveryNotification) },
        { DisplayName: 'Successful Task', keyIsAdminEnabled: 'IsDeliveryAdminSuccessfulTaskEnabled', IsAdminEnabled: this.extractValueByKey(data, "IsDeliveryAdminSuccessfulTaskEnabled", SettingServerGroupTypes.DeliveryNotification), keyAdmin: 'DeliveryAdminSuccessfulTask', valueAdmin: this.extractValueByKey(data, "DeliveryAdminSuccessfulTask", SettingServerGroupTypes.DeliveryNotification), keyIsManagerEnabled: "IsDeliveryManagerSuccessfulTaskEnabled", IsManagerEnabled: this.extractValueByKey(data, "IsDeliveryManagerSuccessfulTaskEnabled", SettingServerGroupTypes.DeliveryNotification), keyManager: 'DeliveryManagerSuccessfulTask', valueManager: this.extractValueByKey(data, "DeliveryManagerSuccessfulTask", SettingServerGroupTypes.DeliveryNotification) },
        { DisplayName: 'Failed Task', keyIsAdminEnabled: 'IsDeliveryAdminFailedTaskEnabled', IsAdminEnabled: this.extractValueByKey(data, "IsDeliveryAdminFailedTaskEnabled", SettingServerGroupTypes.DeliveryNotification), keyAdmin: 'DeliveryAdminFailedTask', valueAdmin: this.extractValueByKey(data, "DeliveryAdminFailedTask", SettingServerGroupTypes.DeliveryNotification), keyIsManagerEnabled: "IsDeliveryManagerFailedTaskEnabled", IsManagerEnabled: this.extractValueByKey(data, "IsDeliveryManagerFailedTaskEnabled", SettingServerGroupTypes.DeliveryNotification), keyManager: 'DeliveryManagerFailedTask', valueManager: this.extractValueByKey(data, "DeliveryManagerFailedTask", SettingServerGroupTypes.DeliveryNotification) },
        { DisplayName: 'Add Note', keyIsAdminEnabled: 'IsDeliveryAdminAddNoteAdminEnabled', IsAdminEnabled: this.extractValueByKey(data, "IsDeliveryAdminAddNoteAdminEnabled", SettingServerGroupTypes.DeliveryNotification), keyAdmin: 'DeliveryAdminAddNoteAdmin', valueAdmin: this.extractValueByKey(data, "DeliveryAdminAddNoteAdmin", SettingServerGroupTypes.DeliveryNotification), keyIsManagerEnabled: "IsDeliveryManagerAddNoteEnabled", IsManagerEnabled: this.extractValueByKey(data, "IsDeliveryManagerAddNoteEnabled", SettingServerGroupTypes.DeliveryNotification), keyManager: 'DeliveryManagerAddNote', valueManager: this.extractValueByKey(data, "DeliveryManagerAddNote", SettingServerGroupTypes.DeliveryNotification) },
        { DisplayName: 'Add Signature', keyIsAdminEnabled: 'IsDeliveryAdminAddSignatureEnabled', IsAdminEnabled: this.extractValueByKey(data, "IsDeliveryAdminAddSignatureEnabled", SettingServerGroupTypes.DeliveryNotification), keyAdmin: 'DeliveryAdminAddSignature', valueAdmin: this.extractValueByKey(data, "DeliveryAdminAddSignature", SettingServerGroupTypes.DeliveryNotification), keyIsManagerEnabled: "IsDeliveryManagerAddSignatureEnabled", IsManagerEnabled: this.extractValueByKey(data, "IsDeliveryManagerAddSignatureEnabled", SettingServerGroupTypes.DeliveryNotification), keyManager: 'DeliveryManagerAddSignature', valueManager: this.extractValueByKey(data, "DeliveryManagerAddSignature", SettingServerGroupTypes.DeliveryNotification) },
        { DisplayName: 'Add Image', keyIsAdminEnabled: 'IsDeliveryAdminAddImageEnabled', IsAdminEnabled: this.extractValueByKey(data, "IsDeliveryAdminAddImageEnabled", SettingServerGroupTypes.DeliveryNotification), keyAdmin: 'DeliveryAdminAddImage', valueAdmin: this.extractValueByKey(data, "DeliveryAdminAddImage", SettingServerGroupTypes.DeliveryNotification), keyIsManagerEnabled: "IsDeliveryManagerAddImageEnabled", IsManagerEnabled: this.extractValueByKey(data, "IsDeliveryManagerAddImageEnabled", SettingServerGroupTypes.DeliveryNotification), keyManager: 'DeliveryManagerAddImage', valueManager: this.extractValueByKey(data, "DeliveryManagerAddImage", SettingServerGroupTypes.DeliveryNotification) },

      ];
    } else if (settingServerGroupTypes == SettingServerGroupTypes.PickupNotification) {
      NotificationsPropertyList = [
        { DisplayName: 'Accept Task', keyIsAdminEnabled: 'IsPickupAdminAcceptTaskEnabled', IsAdminEnabled: this.extractValueByKey(data, "IsPickupAdminAcceptTaskEnabled", SettingServerGroupTypes.PickupNotification), keyAdmin: 'PickupAdminAcceptTask', valueAdmin: this.extractValueByKey(data, "PickupAdminAcceptTask", SettingServerGroupTypes.PickupNotification), keyIsManagerEnabled: "IsPickupManagerAcceptTaskEnabled", IsManagerEnabled: this.extractValueByKey(data, "IsPickupManagerAcceptTaskEnabled", SettingServerGroupTypes.PickupNotification), keyManager: 'PickupManagerAcceptTask', valueManager: this.extractValueByKey(data, "PickupManagerAcceptTask", SettingServerGroupTypes.PickupNotification) },
        { DisplayName: 'Decline Task', keyIsAdminEnabled: 'IsPickupAdminDeclineTaskEnabled', IsAdminEnabled: this.extractValueByKey(data, "IsPickupAdminDeclineTaskEnabled", SettingServerGroupTypes.PickupNotification), keyAdmin: 'PickupAdminDeclineTask', valueAdmin: this.extractValueByKey(data, "PickupAdminDeclineTask", SettingServerGroupTypes.PickupNotification), keyIsManagerEnabled: "IsPickupManagerDeclineTaskEnabled", IsManagerEnabled: this.extractValueByKey(data, "IsPickupManagerDeclineTaskEnabled", SettingServerGroupTypes.PickupNotification), keyManager: 'PickupManagerDeclineTask', valueManager: this.extractValueByKey(data, "PickupManagerDeclineTask", SettingServerGroupTypes.PickupNotification) },
        { DisplayName: 'Start Task', keyIsAdminEnabled: 'IsPickupAdminStartTaskEnabled', IsAdminEnabled: this.extractValueByKey(data, "IsPickupAdminStartTaskEnabled", SettingServerGroupTypes.PickupNotification), keyAdmin: 'PickupAdminStartTask', valueAdmin: this.extractValueByKey(data, "PickupAdminStartTask", SettingServerGroupTypes.PickupNotification), keyIsManagerEnabled: "IsPickupManagerStartTaskEnabled", IsManagerEnabled: this.extractValueByKey(data, "IsPickupManagerStartTaskEnabled", SettingServerGroupTypes.PickupNotification), keyManager: 'PickupManagerStartTask', valueManager: this.extractValueByKey(data, "PickupManagerStartTask", SettingServerGroupTypes.PickupNotification) },
        { DisplayName: 'Cancel Task', keyIsAdminEnabled: 'IsPickupAdminCancelTaskEnabled', IsAdminEnabled: this.extractValueByKey(data, "IsPickupAdminCancelTaskEnabled", SettingServerGroupTypes.PickupNotification), keyAdmin: 'PickupAdminCancelTask', valueAdmin: this.extractValueByKey(data, "PickupAdminCancelTask", SettingServerGroupTypes.PickupNotification), keyIsManagerEnabled: "IsPickupManagerCancelTaskEnabled", IsManagerEnabled: this.extractValueByKey(data, "IsPickupManagerCancelTaskEnabled", SettingServerGroupTypes.PickupNotification), keyManager: 'PickupManagerCancelTask', valueManager: this.extractValueByKey(data, "PickupManagerCancelTask", SettingServerGroupTypes.PickupNotification) },
        { DisplayName: 'Successful Task', keyIsAdminEnabled: 'IsPickupAdminSuccessfulTaskEnabled', IsAdminEnabled: this.extractValueByKey(data, "IsPickupAdminSuccessfulTaskEnabled", SettingServerGroupTypes.PickupNotification), keyAdmin: 'PickupAdminSuccessfulTask', valueAdmin: this.extractValueByKey(data, "PickupAdminSuccessfulTask", SettingServerGroupTypes.PickupNotification), keyIsManagerEnabled: "IsPickupManagerSuccessfulTaskEnabled", IsManagerEnabled: this.extractValueByKey(data, "IsPickupManagerSuccessfulTaskEnabled", SettingServerGroupTypes.PickupNotification), keyManager: 'PickupManagerSuccessfulTask', valueManager: this.extractValueByKey(data, "PickupManagerSuccessfulTask", SettingServerGroupTypes.PickupNotification) },
        { DisplayName: 'Failed Task', keyIsAdminEnabled: 'IsPickupAdminFailedTaskEnabled', IsAdminEnabled: this.extractValueByKey(data, "IsPickupAdminFailedTaskEnabled", SettingServerGroupTypes.PickupNotification), keyAdmin: 'PickupAdminFailedTask', valueAdmin: this.extractValueByKey(data, "PickupAdminFailedTask", SettingServerGroupTypes.PickupNotification), keyIsManagerEnabled: "IsPickupManagerFailedTaskEnabled", IsManagerEnabled: this.extractValueByKey(data, "IsPickupManagerFailedTaskEnabled", SettingServerGroupTypes.PickupNotification), keyManager: 'PickupManagerFailedTask', valueManager: this.extractValueByKey(data, "PickupManagerFailedTask", SettingServerGroupTypes.PickupNotification) },
        { DisplayName: 'Add Note', keyIsAdminEnabled: 'IsPickupAdminAddNoteAdminEnabled', IsAdminEnabled: this.extractValueByKey(data, "IsPickupAdminAddNoteAdminEnabled", SettingServerGroupTypes.PickupNotification), keyAdmin: 'PickupAdminAddNoteAdmin', valueAdmin: this.extractValueByKey(data, "PickupAdminAddNoteAdmin", SettingServerGroupTypes.PickupNotification), keyIsManagerEnabled: "IsPickupManagerAddNoteEnabled", IsManagerEnabled: this.extractValueByKey(data, "IsPickupManagerAddNoteEnabled", SettingServerGroupTypes.PickupNotification), keyManager: 'PickupManagerAddNote', valueManager: this.extractValueByKey(data, "PickupManagerAddNote", SettingServerGroupTypes.PickupNotification) },
        { DisplayName: 'Add Signature', keyIsAdminEnabled: 'IsPickupAdminAddSignatureEnabled', IsAdminEnabled: this.extractValueByKey(data, "IsPickupAdminAddSignatureEnabled", SettingServerGroupTypes.PickupNotification), keyAdmin: 'PickupAdminAddSignature', valueAdmin: this.extractValueByKey(data, "PickupAdminAddSignature", SettingServerGroupTypes.PickupNotification), keyIsManagerEnabled: "IsPickupManagerAddSignatureEnabled", IsManagerEnabled: this.extractValueByKey(data, "IsPickupManagerAddSignatureEnabled", SettingServerGroupTypes.PickupNotification), keyManager: 'PickupManagerAddSignature', valueManager: this.extractValueByKey(data, "PickupManagerAddSignature", SettingServerGroupTypes.PickupNotification) },
        { DisplayName: 'Add Image', keyIsAdminEnabled: 'IsPickupAdminAddImageEnabled', IsAdminEnabled: this.extractValueByKey(data, "IsPickupAdminAddImageEnabled", SettingServerGroupTypes.PickupNotification), keyAdmin: 'PickupAdminAddImage', valueAdmin: this.extractValueByKey(data, "PickupAdminAddImage", SettingServerGroupTypes.PickupNotification), keyIsManagerEnabled: "IsPickupManagerAddImageEnabled", IsManagerEnabled: this.extractValueByKey(data, "IsPickupManagerAddImageEnabled", SettingServerGroupTypes.PickupNotification), keyManager: 'PickupManagerAddImage', valueManager: this.extractValueByKey(data, "PickupManagerAddImage", SettingServerGroupTypes.PickupNotification) },

      ];
    }
    else if (settingServerGroupTypes == SettingServerGroupTypes.GeneralNotification) {
      NotificationsPropertyList = [
        { DisplayName: 'Auto allocation started', keyIsAdminEnabled: 'IsGeneralAdminAutoAllocationStartedEnabled', IsAdminEnabled: this.extractValueByKey(data, "IsGeneralAdminAutoAllocationStartedEnabled", SettingServerGroupTypes.GeneralNotification), keyAdmin: 'GeneralAdminAutoAllocationStarted', valueAdmin: this.extractValueByKey(data, "GeneralAdminAutoAllocationStarted", SettingServerGroupTypes.GeneralNotification), keyIsManagerEnabled: "IsGeneralManagerAutoAllocationStartedEnabled", IsManagerEnabled: this.extractValueByKey(data, "IsGeneralManagerAutoAllocationStartedEnabled", SettingServerGroupTypes.GeneralNotification), keyManager: 'GeneralManagerAutoAllocationStarted', valueManager: this.extractValueByKey(data, "GeneralManagerAutoAllocationStarted", SettingServerGroupTypes.GeneralNotification) },
        { DisplayName: 'Auto allocation failed', keyIsAdminEnabled: 'IsGeneralAdminAutoAllocationFailedEnabled', IsAdminEnabled: this.extractValueByKey(data, "IsGeneralAdminAutoAllocationFailedEnabled", SettingServerGroupTypes.GeneralNotification), keyAdmin: 'GeneralAdminAutoAllocationFailed', valueAdmin: this.extractValueByKey(data, "GeneralAdminAutoAllocationFailed", SettingServerGroupTypes.GeneralNotification), keyIsManagerEnabled: "IsDGeneralManagerAutoAllocationFailedEnabled", IsManagerEnabled: this.extractValueByKey(data, "IsDGeneralManagerAutoAllocationFailedEnabled", SettingServerGroupTypes.GeneralNotification), keyManager: 'GeneralManagerAutoAllocationFailed', valueManager: this.extractValueByKey(data, "GeneralManagerAutoAllocationFailed", SettingServerGroupTypes.GeneralNotification) },
      ];
    }
    return NotificationsPropertyList;

  }
  private extractValueByKey(data, key: string, settingServerGroupTypes: SettingServerGroupTypes): any {
    let element = data.filter(p => p.settingKey == key)[0];
    if (element == undefined) {

      //load defualt data 
      let notificationsSettingPropertes: NotificationsSettingProperty[];
      if (settingServerGroupTypes == SettingServerGroupTypes.PickupNotification) {
        notificationsSettingPropertes = this.DefaultPickupNotificationsEvents();

      } else if (settingServerGroupTypes == SettingServerGroupTypes.DeliveryNotification) {
        notificationsSettingPropertes = this.DefaultDeliveryNotificationsEvents();
      } else if (settingServerGroupTypes == SettingServerGroupTypes.GeneralNotification) {
        notificationsSettingPropertes = this.DefaultGeneralNotification();
      }
      return this.ExtractDefaultValueByKey(notificationsSettingPropertes, key);
    }
    if (element.settingDataType == "boolean") {
      return this.ParsetoBoolean(element.value);
    } else if (element.settingDataType == "string") {
      return JSON.parse(element.value);
    }
  }
  private ParsetoBoolean(value): boolean {
    return JSON.parse(value) == 0 ? false : true;
  }
  private ExtractDefaultValueByKey(notificationsSettingPropertes: NotificationsSettingProperty[], key: string) {
    let keyAdmins = notificationsSettingPropertes.filter(p => p.keyAdmin == key);
    if (keyAdmins != undefined && keyAdmins != null && keyAdmins.length != 0) {
      return keyAdmins[0].valueAdmin;
    }
    let keyManagers = notificationsSettingPropertes.filter(p => p.keyManager == key);
    if (keyManagers != undefined && keyManagers != null && keyManagers.length != 0) {
      return keyManagers[0].valueManager;
    }
    let keyIsAdminsEnabled = notificationsSettingPropertes.filter(p => p.keyIsAdminEnabled == key);
    if (keyIsAdminsEnabled != undefined && keyIsAdminsEnabled != null && keyIsAdminsEnabled.length != 0) {
      return keyIsAdminsEnabled[0].IsAdminEnabled;
    }
    let keyIsManagersEnabled = notificationsSettingPropertes.filter(p => p.keyIsManagerEnabled == key);
    if (keyIsManagersEnabled != undefined && keyIsManagersEnabled != null && keyIsManagersEnabled.length != 0) {
      return keyIsManagersEnabled[0].IsManagerEnabled;
    }
  }


  public UpdateKeyValue(key: string, value: string, settingDataType: string, GroupId: string) {

    //validate data
    if (key == undefined && key == "" && key == null && value == undefined && value == "" && value == null) {
      return
    }

    //call server
    let body: Object = {
      "id": "0",
      "settingKey": key,
      "value": value,
      "settingDataType": settingDataType,
      "GroupId": GroupId
    };
    // let x=JSON.parse(body)
    return this._httpClientService.put(body, `${this.EndPoint}`).subscribe(() => {

    }, () => {

    });
    //  return this._httpClientService.put(body, this.EndPoint);

  }
}

export class NotificationsSettingProperty {

  DisplayName: string;
  keyManager: string;
  valueManager: NotificationsSettingvalues;
  keyIsManagerEnabled: string;
  IsManagerEnabled: boolean;
  keyAdmin: string;
  valueAdmin: NotificationsSettingvalues;
  keyIsAdminEnabled: string;
  IsAdminEnabled: boolean;
}
export enum NotificationsSettingvalues {

  notselected,
  both,
  email,
  sms,

}
export enum SettingServerGroupTypes {
  PickupNotification,
  DeliveryNotification,
  GeneralNotification,

}


export interface SettingServerObject {
  id: string;
  settingDataType: string;
  settingKey: string;
  value: string;
}