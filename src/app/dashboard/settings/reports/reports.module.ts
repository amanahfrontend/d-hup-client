import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportsRoutingModule } from './reports-routing.module';
import { ReportsComponent } from './reports.component';
import { OrderProgressComponent } from './order-progress/order-progress.component';

// material
import {
  MatTableModule,
  MatCheckboxModule,
  MatInputModule,
  MatButtonModule,
  MatIconModule
} from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTooltipModule } from '@angular/material/tooltip';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { LightboxModule } from 'ngx-lightbox';
import { DateToTimezoneModule } from '@dms/app/pipes/date-to-timezone/date-to-timezone.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { LazyloadTranslateModule } from '@dms/app/shared/lazyload-translate/lazyload-translate.module';
import { TruncateTextModule } from '@dms/app/pipes/truncate-text/truncate-text.module';

@NgModule({
  declarations: [ReportsComponent, OrderProgressComponent],
  imports: [
    CommonModule,
    ReportsRoutingModule,

    // material
    MatTableModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatDatepickerModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,
    MatButtonModule,
    MatIconModule,
    NgxPermissionsModule.forChild(),
    MatTooltipModule,

    LightboxModule,
    DateToTimezoneModule,
    LazyloadTranslateModule,
    TruncateTextModule,
  ],
  entryComponents: [
    OrderProgressComponent
  ]
})
export class ReportsModule { }
