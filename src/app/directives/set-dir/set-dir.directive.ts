import { Directive, ElementRef, Inject, LOCALE_ID } from '@angular/core';

@Directive({
  selector: '[setDir]'
})
export class SetDirDirective {

  /**
   * 
   * @param el 
   * @param languageService 
   * @param locale 
   */
  constructor(el: ElementRef,
    @Inject(LOCALE_ID) public locale: string
  ) {
    if (locale == 'ar') {
      el.nativeElement.style.direction = 'rtl';
    }
  }
}
