import { Component, OnInit } from '@angular/core';
import { FacadeService } from '@dms/services/facade.service';
import { Router } from '@angular/router';
import { UserInfo } from '@dms/models/account/userInfo';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class AppHeaderComponent implements OnInit {

  currentState: boolean;
  user: UserInfo;

  /**
   *
   * @param facadeService
   * @param router
   */
  constructor(private facadeService: FacadeService,
    private router: Router) {
  }

  ngOnInit() {
    this.facadeService.authenticatedService.currentStatus.subscribe(state => {
      this.currentState = state;
      this.user = this.facadeService.accountService.user;
    });
  }

  /**
   * clear
   *
   *
   */
  signOut() {
    this.facadeService.accountService.logout();
    this.router.navigate(['auth/login']);
  }
}
