import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { SidebarService } from '@dms/app/services/state-management/sidebar.service';
import { FilterService } from '@dms/app/services/state-management/filter.service';

interface NavItem {
  name: string;
  disabled?: boolean;
  icon: string;
  route?: string;
  permissions?: [];
  children?: NavItem[];
}

@Component({
  selector: 'app-menu-list-item',
  templateUrl: './menu-list-item.component.html',
  styleUrls: ['./menu-list-item.component.scss'],
  animations: [
    trigger('indicatorRotate', [
      state('collapsed', style({ transform: 'rotate(0deg)' })),
      state('expanded', style({ transform: 'rotate(180deg)' })),
      transition('expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
      ),
    ])
  ]
})
export class MenuListItemComponent implements OnInit {

  expanded: boolean = false;
  @HostBinding('attr.aria-expanded') ariaExpanded = this.expanded;
  @Input() item: NavItem;
  @Input() depth: number;

  constructor(public router: Router,
    private sidebarService: SidebarService,
    private filterService: FilterService,
  ) {
    if (this.depth === undefined) {
      this.depth = 0;
    }
  }

  ngOnInit() {

  }

  /**
   * select item
   * 
   * 
   * @param item 
   */
  onItemSelected(item: NavItem) {
    if (item.children && item.children.length > 0) {
      this.expanded = !this.expanded;
    } else {
      this.router.navigate([item.route]);
      this.sidebarService.changeStatus(false);
    }
  }
}
