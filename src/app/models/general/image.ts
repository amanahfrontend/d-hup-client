
export interface Image {
    index?: number;
    src: string;
    type: string;
    uuid?: string
}