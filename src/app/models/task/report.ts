/**
 *
 * @class TaskReport param
 */

export class TaskReportParam {
  constructor(
    public StatusIds: number[],
    public TaskTypeIds: number[],
    public DriversIds: number[],
    public ZonesIds: number[],

    public RestaurantIds: number[],
    public BranchIds: number[],
    public OrderId: string,

    public FromDate: any,
    public ToDate: any,
    public Address: string,

    public pageNumber: number,
    public pageSize: number,
    public searchBy: string,
    public id: number,
    public totalNumbers: number,

    public isOrderProgress: boolean,
  ) { }
}

