import { Injectable, Injector } from '@angular/core';
import { AccountService } from './auth/account.service';
import { RoleService } from './auth/role.service';
import { TeamsService } from './settings/teams.service';
import { LoactionAccuracyService } from './settings/loaction-accuracy.service';
import { AuthenticatedService } from './state-management/authenticated.service';
import { ManagerService } from './settings/manager.service';
import { ManagerAccessControlService } from './settings/access-control/manager-access-control.service';
import { AgentAccessControlService } from './settings/access-control/agent-access-control.service';
import { CountryService } from './settings/country.service';
import { AgentTypeService } from './settings/agent-type.service';
import { TransportTypeService } from './settings/transport-type.service';
import { DriverAccessControlService } from './settings/driver-access-control.service';
import { DriverService } from './settings/driver.service';
import { CustomerService } from './settings/customer.service';
import { MainTaskTypeService } from './main/tasks/main-task-type.service';
import { MainTaskService } from './main/tasks/main-task.service';
import { TaskTypeService } from './main/tasks/task-type.service';
import { SearchService } from './maps/search.service';
import { GeoFenceService } from './settings/geoFence.service';
import { AutoAllocationService } from './settings/auto-allocation.service';
import { TaskStatusService } from './main/tasks/task-status.service';
import { TaskService } from './main/tasks/task.service';
import { RestaurantService } from './settings/restaurant.service';
import { BranchService } from './settings/branch.service';
import { DispatchingManagersService } from './settings/dispatching-managers.service';
import { IpInfoService } from './info/ip-info.service';
import { UserService } from './auth/user.service';
import { DriverLoginRequestService } from './settings/driver-login-request.service';
import { AdminService } from './settings/profile/admin.service';
import { AccountLogService } from './settings/account-log.service';
import { NotificationsService } from './settings/notifications.service';
import { LanguageService } from './translator/language.service';
import { GeneralSettingsService } from './settings/general-settings.service';
import { AddressService } from './settings/address.service';

@Injectable({
  providedIn: 'root'
})

export class FacadeService {

  private _roleService: RoleService;
  private _teamsService: TeamsService;
  private _loactionAccuracyService: LoactionAccuracyService;
  private _authenticatedService: AuthenticatedService;
  private _managerService: ManagerService;
  private _customerService: CustomerService;
  private _agentAccessControlService: AgentAccessControlService;
  private _driverService: DriverService;
  private _countryService: CountryService;
  private _agentTypeService: AgentTypeService;
  private _transportTypeService: TransportTypeService;
  private _driverAccessControlService: DriverAccessControlService;
  private _managerAccessControlService: ManagerAccessControlService;
  private _autoAllocationService: AutoAllocationService;
  private _mainTaskTypeService: MainTaskTypeService;
  private _mainTaskService: MainTaskService;
  private _taskTypeService: TaskTypeService;
  private _taskStatusService: TaskStatusService;
  private _taskService: TaskService;
  private _restaurantService: RestaurantService;
  private _branchService: BranchService;
  private _dispatchingManagersService: DispatchingManagersService;
  private _userService: UserService;
  private _driverLoginRequestService: DriverLoginRequestService;
  private _adminService: AdminService;
  private _accountLogService: AccountLogService;
  private _notificationsService: NotificationsService;
  private _generalSettingsService: GeneralSettingsService;
  private _addressService: AddressService;

  constructor(private inject: Injector) { }

  /**
   * login service
   *
   *
   */
  public get accountService(): AccountService {
    return this.inject.get(AccountService)
  }

  /**
   * role service
   *
   */
  public get roleService(): RoleService {
    if (!this._roleService) {
      this._roleService = this.inject.get(RoleService);
    }
    return this._roleService;
  }

  /**
  * Language service
  *
  */
  public get languageService(): LanguageService {
    return this.inject.get(LanguageService);
  }




  /**
   * teams service
   *
   *
   */
  public get teamsService(): TeamsService {
    if (!this._teamsService) {
      this._teamsService = this.inject.get(TeamsService);
    }
    return this._teamsService;
  }

  /**
   * agentAccessControl service
   *
   *
   */
  public get agentAccessControlService(): AgentAccessControlService {
    if (!this._agentAccessControlService) {
      this._agentAccessControlService = this.inject.get(AgentAccessControlService);
    }
    return this._agentAccessControlService;
  }

  /**
   * managerAccessControl service
   *
   *
   */
  public get managerAccessControlService(): ManagerAccessControlService {
    if (!this._managerAccessControlService) {
      this._managerAccessControlService = this.inject.get(ManagerAccessControlService)
    }
    return this._managerAccessControlService;
  }

  /**
   * locaton accuracy service
   *
   *
   */
  public get loactionAccuracyService(): LoactionAccuracyService {
    if (!this._loactionAccuracyService) {
      this._loactionAccuracyService = this.inject.get(LoactionAccuracyService)
    }
    return this._loactionAccuracyService;
  }

  /**
   * authenticated Service
   *
   *
   */
  public get authenticatedService(): AuthenticatedService {
    if (!this._authenticatedService) {
      this._authenticatedService = this.inject.get(AuthenticatedService)
    }
    return this._authenticatedService;
  }

  /**
   * manager service
   *
   *
   */
  public get ManagerService(): ManagerService {
    if (!this._managerService) {
      this._managerService = this.inject.get(ManagerService)
    }
    return this._managerService;
  }

  /**
   * Dispatching Managers service
   *
   */
  public get dispatchingManagersService(): DispatchingManagersService {
    if (!this._dispatchingManagersService) {
      this._dispatchingManagersService = this.inject.get(DispatchingManagersService)
    }
    return this._dispatchingManagersService;
  }

  /**
   * customer service
   *
   *
   */
  public get customerService(): CustomerService {
    if (!this._customerService) {
      this._customerService = this.inject.get(CustomerService)
    }
    return this._customerService;
  }

  /**
  * Driver service
  *
  *
  */
  public get driverService(): DriverService {
    if (!this._driverService) {
      this._driverService = this.inject.get(DriverService)
    }
    return this._driverService;
  }

  /**
   * Country service
   *
   *
   */
  public get countryService(): CountryService {
    if (!this._countryService) {
      this._countryService = this.inject.get(CountryService)
    }
    return this._countryService;
  }

  /**
  * Agent Type service
  *
  *
  */
  public get agentTypeService(): AgentTypeService {
    if (!this._agentTypeService) {
      this._agentTypeService = this.inject.get(AgentTypeService)
    }
    return this._agentTypeService;
  }

  /**
  * locaton accuracy service
  *
  *
  */
  public get TransportTypeService(): TransportTypeService {
    if (!this._transportTypeService) {
      this._transportTypeService = this.inject.get(TransportTypeService)
    }
    return this._transportTypeService;
  }

  /**
   * driver serivce
   */
  public get DriverAccessControlService(): DriverAccessControlService {
    if (!this._driverAccessControlService) {
      this._driverAccessControlService = this.inject.get(DriverAccessControlService)
    }
    return this._driverAccessControlService;
  }

  public get ManagerAccessControlService(): ManagerAccessControlService {
    if (!this._managerAccessControlService) {
      this._managerAccessControlService = this.inject.get(ManagerAccessControlService)
    }
    return this._managerAccessControlService;
  }

  /**
  * MainTaskType service
  *
  *
  */
  public get mainTaskTypeService(): MainTaskTypeService {
    if (!this._agentTypeService) {
      this._mainTaskTypeService = this.inject.get(MainTaskTypeService)
    }
    return this._mainTaskTypeService;
  }

  /**
   * MainTask service
   * 
   * 
   */
  public get mainTaskService(): MainTaskService {
    if (!this._agentTypeService) {
      this._mainTaskService = this.inject.get(MainTaskService)
    }
    return this._mainTaskService;
  }

  /**
    * TaskType service
    *
    */
  public get taskTypeService(): TaskTypeService {
    if (!this._agentTypeService) {
      this._taskTypeService = this.inject.get(TaskTypeService)
    }
    return this._taskTypeService;
  }

  /**
   * maps service
   *
   *
   */
  public get mapsService(): SearchService {
    return this.inject.get(SearchService)
  }

  /**
  * geo fence service
  *
  *
  */
  public get geoFenceService(): GeoFenceService {
    return this.inject.get(GeoFenceService)
  }

  /**
   * autoAllocation service
   * 
   * 
   */
  public get autoAllocationService(): AutoAllocationService {
    if (!this._autoAllocationService) {
      this._autoAllocationService = this.inject.get(AutoAllocationService)
    }
    return this._autoAllocationService;
  }

  /**
    * Task Status Service
    *
    */
  public get taskStatusService(): TaskStatusService {
    if (!this._taskStatusService) {
      this._taskStatusService = this.inject.get(TaskStatusService)
    }
    return this._taskStatusService;
  }

  /**
   * Task Service
   *
   */
  public get taskService(): TaskService {
    if (!this._taskService) {
      this._taskService = this.inject.get(TaskService)
    }
    return this._taskService;
  }


  /**
    * Restaurant service
    *
    */
  public get restaurantService(): RestaurantService {
    if (!this._restaurantService) {
      this._restaurantService = this.inject.get(RestaurantService)
    }
    return this._restaurantService;
  }

  /**
    * Branch service
    *
    */
  public get branchService(): BranchService {
    if (!this._branchService) {
      this._branchService = this.inject.get(BranchService)
    }
    return this._branchService;
  }
  /**
   * user info sevice
   *
   *
   */
  public get ipInfoService(): IpInfoService {
    return this.inject.get(IpInfoService)
  }

  /**
  * user service
  *
  */
  public get userService(): UserService {
    if (!this._userService) {
      this._userService = this.inject.get(UserService)
    }
    return this._userService;
  }

  public get driverLoginRequestService(): DriverLoginRequestService {
    if (!this._driverLoginRequestService) {
      this._driverLoginRequestService = this.inject.get(DriverLoginRequestService)
    }
    return this._driverLoginRequestService;
  }

  /**
   * admin service
   * 
   * 
   */
  public get adminService(): AdminService {
    if (!this._adminService) {
      this._adminService = this.inject.get(AdminService)
    }
    return this._adminService;
  }

  /**
   * Account Log service
   *
   *
   */
  public get accountLogService(): AccountLogService {
    if (!this._accountLogService) {
      this._accountLogService = this.inject.get(AccountLogService);
    }
    return this._accountLogService;
  }


  /**
   * teams service
   *
   *
   */
  public get notificationsService(): NotificationsService {
    if (!this._notificationsService) {
      this._notificationsService = this.inject.get(NotificationsService);
    }
    return this._notificationsService;
  }

  /**
   * general Settings service
   *
   *
   */
  public get generalSettingsService(): GeneralSettingsService {
    if (!this._generalSettingsService) {
      this._generalSettingsService = this.inject.get(GeneralSettingsService);
    }
    return this._generalSettingsService;
  }

  /**
   * address service
   *
   *
   */
  public get addressService(): AddressService {
    if (!this._addressService) {
      this._addressService = this.inject.get(AddressService);
    }
    return this._addressService;
  }
}
