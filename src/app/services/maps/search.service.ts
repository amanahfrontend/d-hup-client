import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  private static readonly endpoint = 'https://nominatim.openstreetmap.org/search?format=json';
  protected ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(private http: HttpClient) { }

  /**
   * 
   * @param query 
   */
  searchQuery(query: string): Observable<any> {
    if (query.trim() !== '') {
      return this.http.get(SearchService.endpoint, { params: { q: query } })
        .pipe(takeUntil(this.ngUnsubscribe))
    }
  }

  public ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
