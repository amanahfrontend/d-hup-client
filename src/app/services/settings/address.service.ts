import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpClientService } from '@dms/app/core/http-client/http-client.service';
import { SnackBar } from '@dms/app/utilities/snakbar';
import { AddressStreet, Address } from '@dms/models/settings/Address';
import { throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  constructor(private http: HttpClientService, private snackBar: SnackBar) { }

  /**
   * get areas
   *
   *
   * @param id
   */
  get areas() {
    return this.http.get(`Address/GetAllAreas`);
  }


  /**
   * get blocks via area id
   *
   *
   * @param id
   */
  getBlocksByArea(areaId: number) {
    return this.http.get(`Address/get-blocks-from-paci?areaId=${areaId}`);
  }

  /**
   * get streets via governates / area / block name
   *
   *
   * @param id
   */
  getStreetsByBlock(body: AddressStreet) {
    return this.http.get(`Address/get-streets-from-paci`, body);
  }

  /**
   * address using google or paci
   *
   *
   * @param id
   */
  getAddress(body: Address) {
    return this.http.get(`Address/search-by-components`, body);
  }
}
