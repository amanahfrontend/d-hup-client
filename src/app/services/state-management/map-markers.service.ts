import { Injectable } from '@angular/core';
import { MapMarker } from '@dms/app/models/main/tasks/map-marker';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MapMarkersService {

  private mapView: Subject<MapMarker> = new Subject<MapMarker>();
  private markers: Subject<MapMarker[]> = new Subject<MapMarker[]>();
  private driversMarkers: Subject<MapMarker[]> = new Subject<MapMarker[]>();

  public currentMarkers = this.markers.asObservable();
  public currentView = this.mapView.asObservable();
  public currentDriversMarkers = this.driversMarkers.asObservable();

  constructor() {
  }

  changeMarkers(value: MapMarker[], isDriverList: boolean) {
    console.log('markers service', value);

    if (isDriverList) {
      this.driversMarkers.next(value);
    }

    this.markers.next(value);
  }


  changeView(value: MapMarker) {
    this.mapView.next(value);
  }
}
