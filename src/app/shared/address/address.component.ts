import { Component, OnInit, Output, EventEmitter, ViewChild, Input, OnChanges, SimpleChanges, OnDestroy, AfterViewInit, ElementRef } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { FacadeService } from '@dms/app/services/facade.service';
import { debounceTime, distinctUntilChanged, filter, tap } from 'rxjs/operators';
import { Address, AddressStreet, Option } from '@dms/models/settings/Address';
import { MatSelectChange } from '@angular/material';
import { Subscription } from 'rxjs/internal/Subscription';
import { fromEvent } from 'rxjs/internal/observable/fromEvent';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit {

  areas: string[] = [];
  blocks: string[] = [];
  streets: string[] = [];
  tempAreas: Option[] = [];

  address: Address;
  locale: string;

  @Output() addressChanges: EventEmitter<Address> = new EventEmitter<Address>();
  @ViewChild('building', { static: true }) building: ElementRef;

  @Input() addressToReplace: Address;
  @Input() index: number = 0;

  selectedArea: Option;
  selectedBlock: Option;
  subscriptions: Subscription[] = [];

  /**
   * 
   * @param facadeService 
   */
  constructor(private facadeService: FacadeService) {
    this.address = { area: '', block: '', building: '', flat: '', floor: '', street: '' };

    this.subscriptions.push(this.facadeService.languageService.language.subscribe(lngCode => {
      this.locale = lngCode;
    }));
  }

  ngOnInit() {
    this.listAreas();
  }

  ngAfterViewInit() {
    fromEvent(this.building.nativeElement, 'keyup').pipe(filter(Boolean), debounceTime(500), distinctUntilChanged(),
      tap(text => {
        if (this.checkString(this.address.area) && this.checkString(this.address.block) && this.checkString(this.address.street) && this.checkString(this.address.building)) {
          this.address.fullAddress = `${this.locale == 'en' ? 'block' : 'مربع'} ${this.address.block}, ${this.locale == 'en' ? 'building' : 'عمارة'} ${this.address.building} ${this.locale == 'en' ? 'street' : 'شارع'} ${this.address.street}, ${this.address.area}`;
          this.addressChanges.emit(this.address);
        }
      })).subscribe();
  }

  /**
   * check string length
   * 
   * 
   * @param string 
   */
  checkString(string: string): boolean {
    return string && string.trim().length > 0 ? true : false;
  }

  /**
   * areas
   * 
   * 
   */
  listAreas() {
    if (!this.addressToReplace) {
      this.blocks = [];
      this.streets = [];

      this.address.block = '';
      this.address.street = '';
    }

    this.subscriptions.push(this.facadeService.addressService.areas.subscribe((areas: Option[]) => {
      this.tempAreas = areas;
      this.areas = areas.map(area => { return area.name });

      if (this.address.area) {
        this.listBlocksByArea(this.address.area);
      }

      if (this.address.area && this.address.block) {
        const id = this.areaIdFromAreaName(this.address.area);
        this.listStreets({ areaId: id, blockName: this.address.block });
      }
    }));
  }

  /**
   * blocks via area
   * 
   * 
   */
  listBlocksByArea(option: string) {
    if (!this.addressToReplace) {
      this.streets = [];
      this.address.street = '';
    }

    const Id = this.areaIdFromAreaName(option);
    this.subscriptions.push(this.facadeService.addressService.getBlocksByArea(Id).subscribe((blocks: Option[]) => {
      this.blocks = blocks.map(block => { return block.name });
    }));
  }

  /**
   *  blocks via area / block / governate
   * 
   * 
   * @param addressStreet 
   */
  listStreets(addressStreet: AddressStreet) {
    this.subscriptions.push(this.facadeService.addressService.getStreetsByBlock(addressStreet).subscribe((streets: Option[]) => {
      this.streets = streets.map(street => { return street.name });
    }));
  }

  /**
   * 
   * @param changes 
   */
  ngOnChanges(changes: SimpleChanges) {
    if (changes && changes.addressToReplace.currentValue !== null && changes.addressToReplace.currentValue !== undefined) {
      this.address = changes.addressToReplace.currentValue;

      if (this.address.area && this.checkString(this.address.area)) {
        this.listBlocksByArea(this.address.area);
      }

      if (this.address.block && this.checkString(this.address.block)) {
        const id = this.areaIdFromAreaName(this.address.area);
        this.listStreets({ areaId: id, blockName: this.address.block });
      }
    }
  }

  /**
   * select area / block / street according to type
   * 
   * 
   * @param option 
   */
  onSelectOption(option: MatSelectChange, type: string): void {
    const value = option.value;
    if (type == 'area') {
      this.address.area = value;
      this.listBlocksByArea(value);
    }

    if (type == 'block') {
      this.address.block = value;
      this.listStreets({ areaId: this.selectedArea.id, blockName: value });
    }

    if (type == 'street') {
      this.address.street = value;
      if (this.checkString(this.address.area) && this.checkString(this.address.block) && this.checkString(this.address.street) && this.checkString(this.address.building)) {
        this.address.fullAddress = `${this.locale == 'en' ? 'block' : 'مربع'} ${this.address.block}, ${this.locale == 'en' ? 'building' : 'عمارة'} ${this.address.building} ${this.locale == 'en' ? 'street' : 'شارع'} ${this.address.street}, ${this.address.area}`;
        this.addressChanges.emit(this.address);
      }
    }
  }

  /**
   * get area id
   * 
   * 
   * @param areaName 
   */
  areaIdFromAreaName(areaName: string): number {
    this.tempAreas.forEach(area => {
      if (area.name.toLowerCase().includes(areaName.toLowerCase())) {
        this.selectedArea = area;
      }
    });

    return this.selectedArea.id;
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }
}