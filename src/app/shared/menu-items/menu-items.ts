import { Injectable } from '@angular/core';
import { Routes } from '@dms/app/constants/routes';
import { TranslateService } from '@ngx-translate/core';

export interface MenuItem {
  route: string;
  name: string;
  icon: string;
  permissions?: string[];
  children?: MenuItem[];
}

@Injectable()
export class MenuItems {

  constructor(private translateService: TranslateService
  ) {
  }

  menu: MenuItem[] = [
    {
      route: Routes.dashboard,
      name: this.translateService.instant('Dashboard'),
      icon: 'home',
      permissions: [],
    },
    {
      route: Routes.drivers,
      name: this.translateService.instant('Drivers'),
      icon: 'drive_eta',
      permissions: ["ReadAgent"],
    },
    {
      route: Routes.customers,
      name: this.translateService.instant('Customers'),
      icon: 'people_outline',
      permissions: ["ReadCustomer"],
    },
    {
      route: Routes.reports,
      name: this.translateService.instant('Reports'),
      icon: 'playlist_add_check',
      permissions: ["ViewReport"],
    },
    {
      route: Routes.profile,
      name: this.translateService.instant('Profile'),
      icon: 'view_list',
      permissions: [],
    },
    {
      route: Routes.teams,
      name: this.translateService.instant('Teams'),
      icon: 'people',
      permissions: ["ReadMyTeam", "ReadTeam"],
    },
    {
      route: Routes.accessControl,
      name: this.translateService.instant('Access Control'),
      icon: 'accessibility',
      permissions: [
        "Tenant",
        "AddManager",
        "CreateAgent"],
    },
    {
      route: Routes.autoAllocation,
      name: this.translateService.instant('Auto Allocation'),
      icon: 'details',
      permissions: ["UpdateAutoAllocation"],
    },
    {
      route: Routes.geoFence,
      name: this.translateService.instant('Geo Fence'),
      icon: 'rounded_corner',
      permissions: [
        "ReadGeofence",
        "UpdateGeofence",
        "AddGeofence",
        "DeleteGeofence",
      ],
    },
    {
      route: Routes.managers,
      name: this.translateService.instant('Manager'),
      icon: 'important_devices',
      permissions: [
        "AddManager",
        "ReadAllManagers",
        "UpdateAllManager",
        "ReadTeamManager",
        "UpdateTeamManager",
      ],
    },
    // {
    //   route: Routes.notifications,
    //   name: this.translateService.instant('Notifications'),
    //   icon: 'notification_important',
    //   //   permissions: ['ReadNotification', 'UpdateNotification'],
    // },
    {
      route: Routes.logs,
      name: this.translateService.instant('Account logs'),
      icon: 'history',
      permissions: ['Tenant'],
    },
    {
      route: Routes.restaurants,
      name: this.translateService.instant('Restaurants'),
      icon: 'store',
      permissions: [
        "AddRestaurant",
        "ReadRestaurant",
        "UpdateRestaurant",
        "DeleteRestaurant",
      ],
    },
    {
      route: Routes.dispatchingManagers,
      name: this.translateService.instant('Dispatching Managers'),
      icon: 'gavel',
      permissions: [
        "AddManagerDispatching",
        "UpdateManagerDispatching",
        "ReadManagerDispatching",
        "DeleteManagerDispatching",
      ],
    },
    {
      route: Routes.driversLoginRequests,
      name: this.translateService.instant('Drivers Login Requests'),
      icon: 'done_outline',
      permissions: ["ViewDriversLoginRequests"],
    },
    {
      route: Routes.generalSettings,
      name: this.translateService.instant('General Settings'),
      icon: 'perm_data_setting',
      permissions: ["Tenant"],
    },
  ];

  getMenuitem(): MenuItem[] {
    return this.menu;
  }
}
