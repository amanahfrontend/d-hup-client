import { Component, OnInit, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Images } from '@dms/app/constants/images';
import { MapsAPILoader } from '@agm/core';
import { SnackBar } from '@dms/app/utilities';
import { TranslateService } from '@ngx-translate/core';
declare var google: any;

interface CardRouteData {
  driver?: string | null;
  address?: string | null;
  time?: string;
  distance?: string;
  taskType?: string;
}

@Component({
  selector: 'app-task-tracking',
  templateUrl: './task-tracking.component.html',
  styleUrls: ['./task-tracking.component.scss']
})
export class TaskTrackingComponent implements OnInit, AfterViewInit {

  // kuwait
  lat: Number = 29.378586;
  lng: Number = 47.990341;
  queryParams: any;

  origin: any;
  destination: any;

  markerOptions = {
    origin: {
      icon: Images.driverLocation,
    },
    destination: {
      icon: Images.taskLocation,
    },
  }

  renderOptions = {
    suppressMarkers: true,
  }

  cardRouteData: CardRouteData;

  constructor(
    private activatedRoute: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private mapsAPILoader: MapsAPILoader,
    private snackBar: SnackBar,
    private translateService: TranslateService
  ) {
    this.activatedRoute.queryParams.subscribe(queryParams => {
      this.queryParams = queryParams;
    });
  }

  ngOnInit() {
    this.origin = {
      lat: Number(this.queryParams.driverLat),
      lng: Number(this.queryParams.driverLng),
    }

    this.destination = {
      lat: Number(this.queryParams.taskLat),
      lng: Number(this.queryParams.taskLng)
    }

    this.cardRouteData = {
      driver: this.queryParams.driver,
      address: this.queryParams.address,
      taskType: this.queryParams.taskType,
    }

    const origin = `${this.queryParams.driverLat}, ${this.queryParams.driverLng}`;
    const destination = `${this.queryParams.taskLat}, ${this.queryParams.taskLng}`;
    this.distanceAndTime(origin, destination);
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  /**
   * get distance and time between origin & destination
   * 
   * 
   * @param origin 
   * @param destination 
   */
  distanceAndTime(origin: string, destination: string): void {
    this.mapsAPILoader.load().then(() => {
      return new google.maps.DistanceMatrixService().getDistanceMatrix({
        'origins': [origin], 'destinations': [destination],
        travelMode: google.maps.TravelMode.DRIVING
      }, (results: any) => {
        if (results.rows[0].elements[0].status !== "ZERO_RESULTS") {
          this.cardRouteData.distance = results.rows[0].elements[0].distance['text'];
          this.cardRouteData.time = results.rows[0].elements[0].duration['text'];
        } else {
          this.snackBar.openSnackBar({
            message: this.translateService.instant(`something happened and we couldn't detect Estimation`),
            action: this.translateService.instant('okay'),
            duration: 60000
          });
        }
      });
    });
  }

}
