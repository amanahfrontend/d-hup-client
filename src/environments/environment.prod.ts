import { Settings, EnvironmentType } from '@dms/app/core/settings';
import { MapSettings } from '@dms/app/core/map-settings';

/**
 * Base URL settings (development)
 *
 *
 */
const settings: Settings = {
  backendUrl: 'http://37.34.189.202:8030/dms/staging',
  environmentType: EnvironmentType.Production
};

const mapSettings: MapSettings = {
  apiKey: 'AIzaSyCv-ejiH1tmFaGf0uSYUENlHyuTbeMRlws',
  libraries: ['places', 'geometry'],
};

export const environment = {
  production: false,
  settings: settings,
  mapSettings: mapSettings
};
