import { MapSettings } from "@dms/app/core/map-settings";
import { Settings, EnvironmentType } from "@dms/app/core/settings";

/**
 * Base URL settings (development)
 *
 *
 */
const settings: Settings = {
  backendUrl: '',
  environmentType: EnvironmentType.Staging
};

const mapSettings: MapSettings = {
  apiKey: 'AIzaSyCv-ejiH1tmFaGf0uSYUENlHyuTbeMRlws',
  libraries: ['places', 'geometry']
};

export const environment = {
  production: false,
  staging: true,
  settings: settings,
  mapSettings: mapSettings
};
