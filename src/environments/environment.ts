import { MapSettings } from "@dms/app/core/map-settings";
import { Settings, EnvironmentType } from "@dms/app/core/settings";

/**
 * Base URL settings (development)
 *
 *
 */
const settings: Settings = {
  backendUrl: 'http://37.34.189.202:8030/dms/development',
  // backendUrl: 'http://localhost:5000',
  environmentType: EnvironmentType.Development
};

const mapSettings: MapSettings = {
  apiKey: 'AIzaSyCv-ejiH1tmFaGf0uSYUENlHyuTbeMRlws',
  libraries: ['places', 'geometry']
  /**
   * @todo add language code dynamically
   * 
   * 
   */
};

export const environment = {
  production: false,
  settings: settings,
  mapSettings: mapSettings
};
